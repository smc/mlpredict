import random
from collections import defaultdict
import re
import argparse
from typing import List
import json
from graphviz import Digraph

word_split_pattern = re.compile(r"\s+")


class MarkovFST:
    def __init__(self):
        self.transitions = defaultdict(dict)

    def add_transition(self, current_state, next_state):
        if next_state in self.transitions[current_state]:
            self.transitions[current_state][next_state] += 1
        else:
            self.transitions[current_state][next_state] = 1

    def normalize_transitions(self):
        for current_state, next_states in self.transitions.items():
            total = sum(next_states.values())
            for state in next_states:
                next_states[state] /= total

    def generate_text(self, start_state, length=100):

        result = [start_state]
        # Use last word of prompt
        current_state = self.word_split(start_state)[-1]
        for _ in range(length - 1):
            if current_state in self.transitions:
                next_states = list(self.transitions[current_state].keys())
                probabilities = list(self.transitions[current_state].values())
                next_state = random.choices(next_states, weights=probabilities, k=1)[0]
                result.append(next_state)
                current_state = next_state
            else:
                # current_state is a last state
                restarts = ["പക്ഷേ","എന്നാൽ", "അതുകൊണ്ട്", "ഒരിക്കൽ",
                    "അത്", "അവർ", "പിന്നീട്", "ഞാൻ", "സത്യത്തിൽ"]
                current_state=random.choices(restarts)[0]

        return " ".join(result)

    def word_split(self, sentence):
        """
        Splits a sentence into a list of words.
        """
        return re.split(word_split_pattern, sentence)

    def train(self, sentences: List[str]):
        tokenized_text = [self.word_split(sentence.lower()) for sentence in sentences]
        # Assuming `tokenized_text` is a list of lists of words from your dataset
        for sentence in tokenized_text:
            for i in range(len(sentence) - 1):
                self.add_transition(sentence[i], sentence[i + 1])

        markov_fst.normalize_transitions()

    def save(self, outputfile):
        with open(outputfile, "w") as outfile:
            json.dump(self.transitions, outfile, ensure_ascii=False)

    def load(self, model_file):
        with open(model_file, "r") as mf:
            self.transitions = json.loads(mf.read())

    def export_to_graphviz(self, filename="markov_fst.gv"):
        dot = Digraph(comment="Markov FST")

        # Add nodes
        for state in self.transitions:
            dot.node(state, state)

        # Add edges with labels for probabilities
        for current_state, next_states in self.transitions.items():
            for next_state, probability in next_states.items():
                if ":" in current_state or ":" in next_state:
                    # : causes trouble in dot rendering
                    continue
                dot.edge(current_state, next_state, label=str(probability))

        # Save the dot file and render it as a PDF (or other formats)
        dot.render(filename, view=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="markov.fst",
        description="What the program does",
        epilog="Text at the bottom of help",
    )
    parser.add_argument("mode")
    parser.add_argument("-i", "--corpus")
    parser.add_argument("-m", "--model", default="fst.model.json")
    parser.add_argument("-p", "--prompt")
    args = parser.parse_args()

    markov_fst = MarkovFST()

    if args.mode == "train":
        with open(args.corpus, "r") as f:
            corpus = f.read()
            sentences = corpus.split("\n")
            markov_fst.train(sentences)
            markov_fst.save(args.model)

    if args.mode == "generate":
        markov_fst.load(args.model)
        if args.prompt:
            print(markov_fst.generate_text(args.prompt))

    if args.mode == "draw":
        markov_fst.load(args.model)
        markov_fst.export_to_graphviz()