FROM python:3.12

WORKDIR /srv

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential

COPY . /srv/app

RUN pip install --no-cache-dir --upgrade -r /srv/app/requirements.txt

CMD ["uvicorn", "web.main:app", "--host", "0.0.0.0", "--port", "80"]