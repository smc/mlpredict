from fastapi import FastAPI, Request, Response
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from markov_fst import MarkovFST
import json

app = FastAPI()
markov_fst = MarkovFST()
markov_fst.load("fst.model.json")

app.mount("/static", StaticFiles(directory="web/static"), name="static")
templates = Jinja2Templates(directory="web/templates")


@app.get("/", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.post("/api/predict")
async def compile(request: Request) -> Response:
    request_obj: dict = await request.json()
    prompt: str = request_obj["prompt"]
    prompt = prompt.strip()
    completion = markov_fst.generate_text(prompt)
    responsejson: dict = json.dumps(
        {
            "prompt": prompt,
            "completion": completion,
        }
    )

    response: Response = Response(content=responsejson, media_type="application/json")
    return response
