
function doComplete(event) {
    event.preventDefault()
    const prompt = document.getElementById("prompt").value;
    fetch(`/api/predict`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ prompt })
    }).then(response => response.json())
        .then(result => {
            if (!result.completion) {
                document.getElementById('result').innerHTML = 'Error'
                return
            }
            simulateTyping('result', result.completion)
        })
}

function simulateTyping(elementId, text, typingSpeed=10) {
    let element = document.getElementById(elementId);
    let i = 0;
    element.textContent = ""
    function typeCharacter() {
        if (i < text.length) {
            element.textContent += text.charAt(i);
            i++;
            setTimeout(typeCharacter, typingSpeed);
        }
    }

    typeCharacter();
}


document.getElementById("b-complete").addEventListener("click", doComplete);
document.getElementById("form").addEventListener("submit", doComplete);